var User = require('../models/user');

exports.list = function (req, res, next) {
    User.find({}, function (err, users) {
        if (err) {
            return next(err);
        }

        res.json(users);
    });
};

exports.load = function (req, res, next, name) {
    User.findByName(name, function (err, user) {
        if (err) {
            return next(err);
        }

        if (!user) {
            return next(new Error('user not found'));
        }

        res.locals.user = user;
        next();
    });
};

exports.remove = function (req, res, next) {
    res.locals.user.remove(function (err) {
        if (err) {
            return next(err);
        }

        res.status(204).end();
    });
};

exports.create = function (req, res, next) {
    var user = new User(req.body);
    user.save(function (err, user) {
        if (err && err.message === 'duplicate name') {
            res.status(422).json({ error: err.message });
        } else if (err) {
            next(err);
        } else {
            res.json(user);
        }
    });
};

exports.validate = function (req, res, next) {
    res.json(res.locals.user.password == req.body.password);
};