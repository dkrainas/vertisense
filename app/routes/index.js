var router = require('express').Router();
var main = require('../controllers/index');

router.get('/', main.home);
module.exports = router;
