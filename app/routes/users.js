var router = require('express').Router();
var users = require('../controllers/users');

router.get('/users', users.list);
router.param('name', users.load);
router.delete('/users/:name', users.remove);
router.post('/users', users.create);
router.post('/users/:name/validate', users.validate);

module.exports = router;
