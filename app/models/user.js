var _ = require('lodash');

var lookup = {};
var collection = [];

var User = function (obj) {
    this.name = obj.name || null;
    this.password = obj.password || null;
    this.$saved = false;
};

// callbacks are not needed for `save` and `remove`
// but they are added for parity with mongoose
User.prototype.save = function (callback) {
    // null or undefined
    if (this.name == null) {
        return callback(new Error('invalid name'));
    }

    if (!this.$saved && lookup.hasOwnProperty(this.name)) {
        return callback(new Error('duplicate name'));
    }

    this.$saved = true;
    lookup[this.name] = this;
    collection.push(this);
    callback(null, this);
};

User.prototype.remove = function (callback) {
    if (!this.$saved) {
        return callback(null);
    }

    delete lookup[this.name];
    collection.splice(collection.indexOf(this), 1);
    callback(null);
};

User.find = function (query, callback) {
    callback(null, _.where(collection, query));
};

User.findByName = function (name, callback) {
    callback(null, lookup[name]);
};

module.exports = User;