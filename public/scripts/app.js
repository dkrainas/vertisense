!(function () {

    var app = angular.module('app', ['ngRoute']);


    /* Services and Factories */
    app.factory('$confirm', function ($window) {
        return $window.confirm;
    });


    /* Custom Filters */
    app.filter('masked', function () {
        return function (t, mask) {
            return mask && t ? new Array(t.length + 1).join('*') : t;
        };
    });


    /* Controllers */
    app.controller('NavCtrl', function ($scope, $location) {
        $scope.isActive = function (p) {
            return p == $scope.current;
        };

        $scope.go = function (p) {
            $location.path(p);
        };

        $scope.$on('$routeChangeStart', function (event, next, current) {
            $scope.current = next.originalPath;
        });
    });


    app.controller('ValidateCtrl', function ($scope, $http) {
        $scope.user = {};
        $scope.title = 'User Validation';
        $scope.action = 'Validate';

        var invalidUserMsg = {
            type: 'error',
            text: 'Invalid User'
        };

        var validUserMsg = {
            type: 'success',
            text: 'Valid User'
        };

        $scope.submit = function () {
            $http.post('/users/' + $scope.user.name + '/validate', $scope.user)
            .success(function (isValid) {
                $scope.msg = isValid ? validUserMsg : invalidUserMsg;
            })
            .error(function (res, statusCode) {
                if (statusCode == 500) {
                    $scope.msg = {
                        type: 'error',
                        text: res.error
                    };
                } else if (statusCode == 404) {
                    $scope.msg = invalidUserMsg;
                }
            });
        };
    });


    app.controller('NewUserCtrl', function ($scope, $http, $location) {
        $scope.user = {};
        $scope.title = 'New User';
        $scope.action = 'Save';

        var goToUsers = function () {
            $location.path('/users');
        };

        $scope.cancel = goToUsers;

        $scope.submit = function () {
            $http.post('/users', $scope.user)
            .success(goToUsers)
            .error(function (res, statusCode) {
                if (statusCode == 500 || statusCode == 422) {
                    $scope.msg = {
                        type: 'error',
                        text: res.error
                    };
                }
            });
        };
    });


    app.controller('UsersCtrl', function ($scope, $http, $confirm, $location) {
        $http.get('/users')
        .success(function (users) {
            $scope.users = users;
        });

        $scope.isMasked = true;

        $scope.newUser = function () {
            $location.path('/users/new');
        };

        $scope.remove = function (user) {
            if ($confirm('Remove `' + user.name + '`?')) {
                $http.delete('/users/' + user.name)
                    .success(function () {
                        $scope.users.splice($scope.users.indexOf(user), 1);
                    });
            }
        };
    });


    /* Route Configuration */
    app.config(function ($routeProvider) {
        $routeProvider
        .when('/validate', {
            controller: 'ValidateCtrl',
            templateUrl: '/views/form.html'
        })
        .when('/users', {
            controller: 'UsersCtrl',
            templateUrl: '/views/users.html',
        })
        .when('/users/new', {
            controller: 'NewUserCtrl',
            templateUrl: '/views/form.html'
        })
        .otherwise({ redirectTo: '/validate' });
    });
})();