process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var express = require('express');
var app = express();

var config = require('./config/config');
require('./config/express')(app);

app.listen(config.port);
console.log('%s(%s) started on port %s', config.app.name, config.env, config.port);
module.exports = app;
