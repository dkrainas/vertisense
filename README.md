# Vertisense Users

Test solution for **Vertisense NodeJS Practicum**

## Installation

Install the dependencies via the `npm` command by running: 

```
npm install
```

## How to Run

To run the application, execute `server.js` by:

```
node server.js
```

or by using the `npm` command:

```
npm start
```
