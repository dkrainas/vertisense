var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var compression = require('compression');
var fs = require('fs');
var config = require('./config');
var exphbs  = require('express-handlebars');

var loadRoutes = function () {
    var router = express.Router();
    var routes_path = __dirname + '/../app/routes';
    var walk = function(path) {
        fs.readdirSync(path).forEach(function(file) {
            var newPath = path + '/' + file;
            var stat = fs.statSync(newPath);
            if(stat.isFile() && /(.*).(js$)/.test(file)) {
                try {
                    router.use(require(newPath));
                } catch (err) {
                    console.error('#ERROR could not load routes: %s', newPath);
                    console.error(err || '');
                }
            }else if(stat.isDirectory()) {
                walk(newPath);
            }
        });
    };

    walk(routes_path);
    return router;
};

module.exports = function (app) {
    app.set('showStackError', true);
    app.locals.app = config.app;

    app.use(compression({
        filter: function (req, res) {
            return (/json|text|javascript|css/).test(res.getHeader('Content-Type'));
        },

        level: 9
    }));

    app.use(morgan('dev'));
    
    app.engine('hbs', exphbs());
    app.set('view engine', 'hbs');
    app.set('views', config.root + '/app/views');

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.use(loadRoutes());
    app.use(express.static(config.root + '/public'));

    app.use(function (err, req, res, next) {
        if (~err.message.indexOf('not found')) {
            return next();
        }

        console.error(err.stack);

        res.status(500);
        var data = { error: err };
        res.format({
            html: function () {
                res.send(JSON.stringify(data));
            },
            json: function () {
                res.json(data);
            }
        })
    });

    app.use(function (req, res) {
        var data = { url: req.originalUrl };
        res.status(404);
        res.format({
            html: function () {
                res.send(JSON.stringify(data));
            },
            json: function () {
                res.json(data);
            }
        });
    });
};