var path = require('path');
var rootPath = path.normalize(__dirname + '/..');

module.exports = {
    env: process.env.NODE_ENV,
    
    root: rootPath,

    app: {
        name: 'Vertisense Users',
    },

    port: process.env.PORT || 6200
};
